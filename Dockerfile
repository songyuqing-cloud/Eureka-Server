FROM openjdk:8
WORKDIR /

COPY target/Eureka-Server-0.0.1-SNAPSHOT.jar ./Eureka-Server-0.0.1-SNAPSHOT.jar

EXPOSE 8761

ENTRYPOINT ["java","-jar","/Eureka-Server-0.0.1-SNAPSHOT.jar"]